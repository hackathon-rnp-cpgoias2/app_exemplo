import 'package:base_eduplay/base_eduplay.dart';
import 'package:bloc/bloc.dart';
import 'package:eduplay/app/modules/shared/repository/transmission_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

class LiveCubit extends Cubit<List<Transmission>> implements Disposable {
  LiveCubit(this._transmissionRepository) : super([]);

  final TransmissionRepository _transmissionRepository;

  fetchLives() {
    _transmissionRepository.liveNow()
        .then((value) => emit(value))
        .onError((error, stackTrace) => addError(error ?? 'Erro', stackTrace));
  }

  @override
  void dispose() {
  }

}
