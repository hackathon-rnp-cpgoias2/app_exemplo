import 'package:base_eduplay/base_eduplay.dart';
import 'package:bloc/bloc.dart';

class VideoVersionCubit extends Cubit<VideoVersion> {
  VideoVersionCubit(this._repository) : super(VideoVersion());

  final VideoRepository _repository;


  fetchVersionFor({required Video video}) {
    _repository.versionFor(videoId: video.id!)
        .then((value) => emit(value ?? VideoVersion()));
  }

  registerPlayback({required int forVideoId}) {
    _repository.registerPlayback(forVideoId);
  }

}
