import 'package:base_eduplay/base_eduplay.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

class VideoCubit extends Cubit<List<Video>> implements Disposable {
  final VideoRepository _repository;

  VideoCubit(this._repository): super([]);


  fetchVideos(SortType sortType) {
    _repository.fetchVideos(sort: sortType)
        .then((value) => emit(value))
        .onError((error, stackTrace) => addError(error ?? 'Erro', stackTrace));
  }

  @override
  void dispose() {
  }

}
