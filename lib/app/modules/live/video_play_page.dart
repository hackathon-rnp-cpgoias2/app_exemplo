import 'dart:developer';

import 'package:base_eduplay/base_eduplay.dart';
import 'package:chewie/chewie.dart';
import 'package:eduplay/app/modules/live/bloc/video_version_cubit.dart';
import 'package:eduplay/app/modules/shared/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:video_player/video_player.dart';
import 'package:simple_html_css/simple_html_css.dart';

class VideoPlayPage extends StatefulWidget {
  final Video video;
  const VideoPlayPage({Key? key, required this.video}) : super(key: key);

  @override
  VideoPlayPageState createState() => VideoPlayPageState();

}
class VideoPlayPageState extends ModularState<VideoPlayPage, VideoVersionCubit> {

  late VideoPlayerController _videoPlayerController;
  bool playbackRegistered = false;

  @override
  void initState() {
    super.initState();
    cubit.fetchVersionFor(video: widget.video);
  }

  @override
  void deactivate() {
    super.deactivate();
    try {
      _videoPlayerController.pause();
    }catch (e) {
      log('Erro ao pausar', error: e);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _videoPlayerController.dispose();
  }

  void _playbackStatusChanged() {
    if (_videoPlayerController.value.isPlaying) {
      if (!playbackRegistered) {
        cubit.registerPlayback(forVideoId: widget.video.id!);
        playbackRegistered = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: Container(
          width: 64,
          height: 64,
          margin: const EdgeInsets.all(7),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Theme.of(context).bottomNavigationBarTheme.backgroundColor
          ),
          child: InkWell(
            onTap: () => Navigator.pop(context),
            child: const Icon(Icons.arrow_back_rounded, size: 21),
          ),
        ),
        title: Flexible(
          child: Text(widget.video.title ?? 'Sem Título', maxLines: 2, overflow: TextOverflow.ellipsis, style: const TextStyle(fontFamily: 'BasikBook', fontWeight: FontWeight.w500)),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                boxShadow: kElevationToShadow[2],
                borderRadius: const BorderRadius.all(Constants.circularRadius)
              ),
              clipBehavior: Clip.antiAliasWithSaveLayer,
              margin: const EdgeInsets.all(14),
              child: BlocBuilder(
                bloc: bloc,
                builder: (context, VideoVersion version) {
                  if (version.url == null) {
                    return Container();
                  }
                  _videoPlayerController = VideoPlayerController.network(
                      version.url!,
                      videoPlayerOptions: VideoPlayerOptions(allowBackgroundPlayback: true)
                  );
                  _videoPlayerController.addListener(_playbackStatusChanged);

                  final colors = ChewieProgressColors(
                      backgroundColor: Theme.of(context).backgroundColor,
                      bufferedColor: Theme.of(context).primaryColor,
                      handleColor: Theme.of(context).primaryColorLight,
                      playedColor: Theme.of(context).primaryColorDark
                  );
                  final chewieController = ChewieController(
                      videoPlayerController: _videoPlayerController,
                      deviceOrientationsAfterFullScreen: [DeviceOrientation.portraitUp],
                      deviceOrientationsOnEnterFullScreen: [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight],
                      allowMuting: true,
                      autoPlay: false,
                      looping: false,
                      showControlsOnInitialize: true,
                      allowedScreenSleep: false,
                      showControls: true,
                      showOptions: true,
                      allowFullScreen: true,
                      aspectRatio: (version.frameWidth ?? 1.0) / (version.frameHeight ?? 1.0),
                      zoomAndPan: true,
                      materialProgressColors: colors,
                      cupertinoProgressColors: colors,
                      errorBuilder: (context, text) => Container(
                        margin: const EdgeInsets.all(Constants.horizontalPadding),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Flexible(child: Text('A mídia não pode ser carregada, por uma falha de rede ou servidor ou o formato não é suportado.', style: Theme.of(context).textTheme.bodyLarge)),
                            Flexible(child: Text(text, style: Theme.of(context).textTheme.caption))
                          ],
                        ),
                      ),

                  );
                  return FutureBuilder(
                    future: _videoPlayerController.initialize(),
                    builder: (context, _) => Chewie(
                      controller: chewieController,
                    ),
                  );
                },
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              padding: const EdgeInsets.all(Constants.horizontalPadding),
              child: ListView(
                children: [
                  HTML.toRichText(
                    context,
                    widget.video.description ?? '',
                    defaultTextStyle: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
