import 'package:base_eduplay/base_eduplay.dart';
import 'package:eduplay/app/modules/live/bloc/live_cubit.dart';
import 'package:eduplay/app/modules/live/bloc/video_cubit.dart';
import 'package:eduplay/app/modules/live/bloc/video_version_cubit.dart';
import 'package:eduplay/app/modules/live/live_page.dart';
import 'package:eduplay/app/modules/live/video_play_page.dart';
import 'package:eduplay/app/modules/shared/repository/transmission_repository.dart';
import 'package:eduplay/app/modules/shared/shared_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

class LiveModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => VideoCubit(i.get<VideoRepository>())),
    Bind.lazySingleton((i) => VideoVersionCubit(i.get<VideoRepository>())),
    Bind.lazySingleton((i) => LiveCubit(i.get<TransmissionRepository>())),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(
      Modular.initialRoute,
      child: (context, args) => const LivePage(),
    ),
    ChildRoute('/play',
        child: (context, args) => VideoPlayPage(video: args.data),
        transition: TransitionType.rightToLeftWithFade),
  ];

  @override
  List<Module> get imports => [
        ...super.imports,
        SharedModule(),
      ];
}
