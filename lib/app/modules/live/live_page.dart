import 'package:eduplay/app/modules/live/widgets/live_list_widget.dart';
import 'package:eduplay/app/modules/live/widgets/video_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class LivePage extends StatefulWidget {
  const LivePage({Key? key}) : super(key: key);
  @override
  LivePageState createState() => LivePageState();
}
class LivePageState extends State<LivePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('eduplay', style: TextStyle(fontFamily: 'BasikBook', fontWeight: FontWeight.w500, fontSize: 32)),
        backgroundColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Expanded(
              child: LiveListWidget()
            ),
            Expanded(
              child: VideoListWidget(
                itemTap: (video) {
                  Modular.to.pushNamed('/live/play', arguments: video);
                },
              )
            )
          ],
        ),
      ),
    );
  }
}
