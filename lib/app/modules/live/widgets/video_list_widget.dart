import 'package:base_eduplay/base_eduplay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eduplay/app/modules/live/bloc/video_cubit.dart';
import 'package:eduplay/app/modules/shared/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

typedef VideoItemTapCallback = void Function(Video video);

class VideoListWidget extends StatefulWidget {
  final SortType sort;
  final VideoItemTapCallback itemTap;
  const VideoListWidget({Key? key, this.sort = SortType.RECENT, required this.itemTap}) : super(key: key);

  @override
  State<StatefulWidget> createState() => VideoListState();

}

class VideoListState extends ModularState<VideoListWidget, VideoCubit> {

  String _sortTypeTranslator() {
    switch (widget.sort) {
      case SortType.RELEVANT: return 'Populares';
      case SortType.LIKES: return 'Mais Curtidos';
      case SortType.VIEWS: return 'Mais Vistos';
      case SortType.RECENT: return 'Mais Recentes';
      case SortType.OLD: return 'Mais Antigos';
    }
  }

  @override
  void initState() {
    cubit.fetchVideos(widget.sort);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: cubit,
      builder: (context, List<Video> items) {
        if (items.isEmpty) {
          return Container();
        }
        return Container(
          decoration: BoxDecoration(
              color: Theme.of(context).bottomNavigationBarTheme.backgroundColor,
              borderRadius: const BorderRadius.only(
                  topLeft: Constants.doubleCircularRadius,
                  topRight: Constants.doubleCircularRadius
              )
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: Constants.horizontalPadding * 1.5, vertical: Constants.verticalPadding * 1.5),
                child: Text('Vídeos ${_sortTypeTranslator()}', style: Theme.of(context).textTheme.headline6!.copyWith(fontFamily: 'BasikBook', color: Theme.of(context).primaryColorLight)),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(Constants.horizontalPadding),
                  child: GridView.count(
                    crossAxisCount: 3,
                    crossAxisSpacing: Constants.verticalPadding,
                    mainAxisSpacing: Constants.horizontalPadding,
                    children: items.map((video) {
                      return _VideoItemWidget(video, itemTap: widget.itemTap);
                    }).toList(growable: false),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class _VideoItemWidget extends StatelessWidget {

  final Video video;
  final VideoItemTapCallback itemTap;
  const _VideoItemWidget(this.video, {required this.itemTap});

  @override
  Widget build(BuildContext context) => InkWell(
    onTap: () => itemTap(video),
    child: Container(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Constants.circularRadius),
        boxShadow: kElevationToShadow[2],
        image: DecorationImage(
          image: CachedNetworkImageProvider((video.images ?? []).isNotEmpty ? video.images!.first.href! : 'https://i.ytimg.com/vi/ZIV76TaQXwM/maxresdefault.jpg'),
          isAntiAlias: true,
          alignment: Alignment.center,
          filterQuality: FilterQuality.medium,
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
            gradient: RadialGradient(
                colors: [Colors.black.withAlpha(50), Colors.transparent],
                radius: 0.3,
                focalRadius: 0.2
            )
        ),
        child: Icon(
          Icons.play_arrow_rounded,
          size: 64,
          color: Colors.white.withAlpha(180),
        ),
      ),
    ),
  );
}
