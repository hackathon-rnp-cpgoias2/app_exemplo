import 'dart:math';

import 'package:base_eduplay/base_eduplay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eduplay/app/modules/live/bloc/live_cubit.dart';
import 'package:eduplay/app/modules/shared/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:infinite_carousel/infinite_carousel.dart';

class LiveListWidget extends StatefulWidget {
  const LiveListWidget({Key? key}): super(key: key);

  @override
  State<StatefulWidget> createState() => LiveListState();

}

class LiveListState extends ModularState<LiveListWidget, LiveCubit> {
  late final _flexKey = const ValueKey('flex');

  @override
  void initState() {
    cubit.fetchLives();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: Constants.horizontalPadding * 1.5),
          child: Text('Ao vivo Agora', style: Theme.of(context).textTheme.headline6!.copyWith(fontFamily: 'BasikBook', color: Theme.of(context).brightness == Brightness.dark ? Theme.of(context).primaryColorLight : Theme.of(context).primaryColorDark)),
        ),
        const SizedBox(height: Constants.verticalPadding),
        Flexible(
          key: _flexKey,
          child: BlocBuilder(
            bloc: cubit,
            builder: (context, List<Transmission> items) {
              if (items.isEmpty) {
                return Container();
              }
              return InfiniteCarousel.builder(
                  itemCount: items.length,
                  itemExtent: MediaQuery.of(context).size.width * 0.8,
                  center: true,
                  velocityFactor: 0.2,
                  onIndexChanged: (index) {

                  },
                  itemBuilder: (context, itemIndex, realIndex) {
                    final item = items[itemIndex];
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: Constants.horizontalPadding / 2),
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(Constants.circularRadius),
                            boxShadow: kElevationToShadow[2],
                            image: DecorationImage(
                              image: CachedNetworkImageProvider('http://lorempixel.com.br/800/600?${item.id}'),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: Theme.of(context).brightness == Brightness.dark ?
                                  BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.bottomCenter,
                                          end: Alignment.topCenter,
                                          colors: [
                                            Theme.of(context).primaryColorDark,
                                            Colors.transparent
                                          ]
                                      )
                                  )
                                  :
                                  BoxDecoration(
                                    border: Border.all(width: 0),
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                borderRadius: const BorderRadius.all(Radius.circular(100)),
                                                color: Theme.of(context).primaryColor,
                                            ),
                                            margin: const EdgeInsets.all(Constants.horizontalPadding / 2),
                                            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                            child: Text('${Random.secure().nextInt(100)} assistindo', style: Theme.of(context).textTheme.caption!.copyWith(color: Colors.white)),
                                          )
                                        ],
                                      ),
                                      const Spacer(),
                                      Expanded(
                                        flex: 0,
                                        child: Container(
                                          color: Theme.of(context).brightness == Brightness.dark ? Colors.transparent : Theme.of(context).primaryColorLight,
                                          padding: const EdgeInsets.all(Constants.horizontalPadding),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: [
                                              Text(item.title ?? 'Sem título', style: Theme.of(context).textTheme.subtitle1),
                                              Text(item.description ?? 'Sem descrição', style: Theme.of(context).textTheme.caption, maxLines: 2, overflow: TextOverflow.ellipsis),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }
              );
            },
          ),
        ),
        const SizedBox(height: Constants.verticalPadding),
      ],
    );
  }
}
