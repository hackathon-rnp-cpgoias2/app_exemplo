import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _onTabChange(_currentIndex);
  }

  void _onTabChange(int index) {
    setState(() {
      _currentIndex = index;
    });
    switch (index) {
      case 0: Modular.to.navigate('/live/'); break;
      case 1: Modular.to.navigate(''); break;
      case 2: Modular.to.navigate(''); break;
      case 3: Modular.to.navigate(''); break;
      default: break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const RouterOutlet(),
      bottomNavigationBar: BottomNavigationBar(
        enableFeedback: true,
        type: BottomNavigationBarType.fixed,
        onTap: _onTabChange,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.now_widgets_outlined),
            activeIcon: Icon(Icons.now_widgets),
            label: ''
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tv_outlined),
            activeIcon: Icon(Icons.tv),
              label: ''
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.music_note_outlined),
              activeIcon: Icon(Icons.music_note),
              label: ''
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_outlined),
              activeIcon: Icon(Icons.person),
              label: ''
          ),
        ],
      ),
    );
  }
}