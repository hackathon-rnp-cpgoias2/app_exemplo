import 'package:base_eduplay/base_eduplay.dart';
import 'package:dio/dio.dart';
import 'package:eduplay/app/modules/shared/repository/transmission_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SharedModule extends Module {
  @override
  List<Bind> get binds => [
    Bind.factory((i) => TransmissionRepository(i.get<Dio>()), export: true),
    Bind.factory((i) => VideoRepository(i.get<Dio>()), export: true),
  ];
}
