import 'package:eduplay/app/modules/shared/utils/constants.dart';
import 'package:flutter/material.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  final List<BottomNavigationBarItem>? items;
  const BottomNavigationBarWidget({Key? key, this.items}) : super(key: key);


  @override
  Widget build(BuildContext context) => BottomNavigationBar(
    items: items ?? [],



  );
  // Widget build(BuildContext context) => Container(
  //   decoration: const BoxDecoration(
  //     color: Colors.white,
  //     borderRadius: BorderRadius.all(Constants.circularRadius)
  //   ),
  //   height: 60,
  //   padding: const EdgeInsets.symmetric(horizontal: Constants.horizontalPadding),
  //   margin: EdgeInsets.symmetric(horizontal: Constants.horizontalPadding, vertical: Constants.verticalPadding/4 + (MediaQuery.of(context).padding.bottom > 0 ? MediaQuery.of(context).padding.bottom / 2 : 0)),
  // );
}