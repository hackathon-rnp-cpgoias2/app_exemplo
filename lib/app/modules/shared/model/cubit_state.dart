
import 'package:base_eduplay/base_eduplay.dart';

class CubitState<State> {

  CubitStatus status;
  State? result;
  List<State>? results;
  Error? error;

  CubitState({
    this.status = CubitStatus.unknown,
    this.result,
    this.results,
    this.error
  });

}
