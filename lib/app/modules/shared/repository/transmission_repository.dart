import 'package:base_eduplay/base_eduplay.dart';
import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';

class TransmissionRepository extends Disposable {

  final Dio _dio;
  TransmissionRepository(this._dio);

  Future<List<Transmission>> liveNow() async {
    final dateFormat = DateFormat("yyyy-MM-dd");
    final from = dateFormat.format(DateTime.now().subtract(const Duration(days: 7)));
    final until = dateFormat.format(DateTime.now());
    final response = await _dio.get('/transmission', queryParameters: {'offet': 0, 'limit': 10});
    if (response.statusCode != null && response.statusCode! >= 200 && response.statusCode! < 300) {
      final page = TransmissionResult.fromJson(response.data);
      return page.items ?? [];
    }
    return [];
  }

  @override
  void dispose() {

  }

}
