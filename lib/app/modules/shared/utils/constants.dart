import 'package:flutter/widgets.dart';

class Constants {

  static const double horizontalPadding = 14;
  static const double verticalPadding = 14;
  static const Radius circularRadius = Radius.circular(14);
  static const Radius doubleCircularRadius = Radius.circular(28);

}