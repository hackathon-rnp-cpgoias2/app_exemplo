import 'package:base_eduplay/base_eduplay.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'modules/home/home_module.dart';

class AppModule extends Module {

  @override
  List<Bind> get binds => [
    Bind.factory((i) => eduplayDio)
  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(Modular.initialRoute, module: HomeModule()),
  ];

}
