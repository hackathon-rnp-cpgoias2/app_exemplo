import 'package:eduplay/app/modules/shared/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppWidget extends StatelessWidget {

  static const eduplaySwatch = MaterialColor(
      0xff3775b7,
      {
        50: Color(0xffe4f1f9),
        100: Color(0xffbeddf2),
        200: Color(0xff98c8ea),
        300: Color(0xff74b2e1),
        400: Color(0xff5ba2dc),
        500: Color(0xff4894d6),
        600: Color(0xff4086c9),
        700: Color(0xff3775b7),
        800: Color(0xff3064a5),
        900: Color(0xff244885)
      }
  );

  AppWidget({Key? key}) : super(key: key);

  get eduplayTheme => ThemeData(
    useMaterial3: true,
    fontFamily: 'BasikBook',
    brightness: Brightness.light,
    primarySwatch: eduplaySwatch,
    primaryColorLight: eduplaySwatch.shade100,
    primaryColorDark: eduplaySwatch.shade900,
    appBarTheme: AppBarTheme(
      elevation: 0,
      backgroundColor: eduplaySwatch.shade300,
      titleTextStyle: TextStyle(
        color: eduplaySwatch.shade900,
        fontSize: 20,
        fontWeight: FontWeight.w600
      )
    ),
    scaffoldBackgroundColor: eduplaySwatch.shade50,
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      elevation: 0,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      backgroundColor: eduplaySwatch.shade500,
      selectedItemColor: const Color(0xffE79A4B),
      unselectedItemColor: eduplaySwatch.shade300,
      enableFeedback: true,
    ),
    inputDecorationTheme: InputDecorationTheme(
      alignLabelWithHint: true,
      iconColor: eduplaySwatch.shade900,
      filled: true,
      fillColor: eduplaySwatch.shade100,
      focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Constants.circularRadius),
          borderSide: BorderSide(color: eduplaySwatch.shade100)
      ),
      enabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Constants.circularRadius),
          borderSide: BorderSide(color: Colors.transparent)
      ),
      disabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Constants.circularRadius),
          borderSide: BorderSide(color: Colors.transparent)
      ),
      labelStyle: TextStyle(color: eduplaySwatch.shade900),
      hintStyle: TextStyle(color: eduplaySwatch.shade200),
      errorStyle: const TextStyle(color: Colors.redAccent),
    ),
    textTheme: ThemeData.light().textTheme,
  );

  final theme = ThemeData(
    useMaterial3: true,
    brightness: Brightness.dark,
    primarySwatch: Colors.deepPurple,
    primaryColorDark: const Color(0xff262040),
    primaryColorLight: Colors.deepPurple.shade50,
    primaryColor: Colors.deepPurple,
    fontFamily: 'BasikBook',
    appBarTheme: const AppBarTheme(
      elevation: 0,
      backgroundColor: Color(0xff322E56)
    ),
    scaffoldBackgroundColor: const Color(0xff262040),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      elevation: 0,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      backgroundColor: Color(0xff322E56),
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white30,
      enableFeedback: true,
    ),
    inputDecorationTheme: const InputDecorationTheme(
      alignLabelWithHint: true,
      iconColor: Colors.white,
      filled: true,
      fillColor: Color(0xff322E56),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Constants.circularRadius),
          borderSide: BorderSide(color: Colors.white30)
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Constants.circularRadius),
          borderSide: BorderSide(color: Colors.transparent)
      ),
      disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Constants.circularRadius),
          borderSide: BorderSide(color: Colors.transparent)
      ),
      labelStyle: TextStyle(color: Colors.white),
      hintStyle: TextStyle(color: Colors.white30),
      errorStyle: TextStyle(color: Colors.redAccent),
    ),
    textTheme: ThemeData.dark().textTheme,
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      // debugShowCheckedModeBanner: false,
      routerDelegate: Modular.routerDelegate,
      routeInformationParser: Modular.routeInformationParser,
      title: 'Flutter Slidy',
      theme: eduplayTheme,
      darkTheme: theme,
      themeMode: ThemeMode.system
    );
  }
}