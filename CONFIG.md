<img src="assets/flutter_logo.png" width="80" />


# Guia de Configuração do Ambinete de Desenvolvimento

# Pré-requisitos
- 
- Java JDK 11+
- [Flutter](https://docs.flutter.dev/get-started/install)
- [Android Studio](https://developer.android.com/studio?hl=pt)
- Android SDK
- [Xcode](https://developer.apple.com/xcode/) (somente para macOS)
- [GIT](https://git-scm.com/)

# Instalação do JAVA

Para iniciar, devemos instalar o JDK 11, seguindo os comandos abaixo conforme o sistema operacional que estiver usando:

## Linux 🐧

Os comandos para o Linux, foram baseados na distribuição Ubuntu `22.04 LTS`.

Antes de iniciar a instalação do Java vamos garantir que os pacotes do Linux estão atualizados, para isso abra o terminal e execute o comando abaixo:

```shell
sudo apt-get update
```

Após a atualização dos pacotes, execute o comando abaixo para instalação do JDK 11:

```shell
sudo apt-get install openjdk-11-jdk
```

Para garantir que o Java foi instalado com sucesso, executar o comando a seguir. Caso o terminal não reconheça o comando, basta fechar o terminal e abrir novamente.

```shell
java --version
```

Pronto! O Java foi instalado na sua máquina.

## macOS 🍎

No macOS, adotamos o gerenciador de pacotes [Homebrew](https://brew.sh/index_pt-br). Todos os comandos a seguir consideram que o Homebrew está instalado e pronto para uso.

Para instalar o Homebrew, abra o terminal e execute o comando abaixo.

```shell
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Após a instalação do Homebrew, execute o comando para garantir que o cache local do Homebrew está atualizado. Caso o terminal não reconheça o comando basta fechar o terminal e abrir novamente.

```shell
brew update
```

Após a atualização dos pacotes, execute o comando abaixo para instalação do JDK 11:

```shell
brew install openjdk@11
```

Para garantir que o Java foi instalado com sucesso basta executar o comando, caso o terminal não reconheça o comando basta fechar o terminal e abrir novamente.

```shell
java --version
```

Pronto! O Java foi instalado na sua máquina.

## Windows

Antes de iniciar a instalação do Java no Windows, recomendamos o uso de um gerenciador de pacotes. Abaixo iremos mostrar como instalar o gerenciador de pacotes [Cocolatey](https://chocolatey.org).

Para Instalar o gerenciador de pacotes Chocolatey, acesse a página [https://chocolatey.org/install](http:s//chocolatey.org/install) e siga os passos para instalação.

Após a instalação, execute o **prompt de comando (CMD)** ou **PowerShell** como administrador

Verifique se o Chocolatey foi realmetne instalado executando o comando abaixo.

```shell
choco
```

Para listar todas as opções que o Chocolatey posssui, execute o comando: `choco /?`

### Instale o Java JDK 11

Após a instalação do Chocolatey, execute o comando abaixo para instalação do JDK 11:

```shell
choco install openjdk11
```

Para garantir que o Java foi instalado com sucesso, feche o prompt de comando e abra novamente e execute o comando abaixo.

```shell
java --version
```

Pronto! O Java foi instalado na sua máquina.

## Configurando variáveis de ambiente para o JAVA

>**ATENÇÃO**: Esse passo nem sempre é necessário. Verifique a existência e o preenchimento das variáveis mencionadas a seguir **antes** de executar na sua máquia os comandos propostos.

Dependendo da sua instalação do Java, pode ser necessário a criação da variável de ambiente `JAVA_HOME` e adicionar o Java no Path (no caso do Windows), para isso siga os passos abaixo.

### Linux

Crie a variável de ambiente `JAVA_HOME` no `~/.bash_profile` ou `~/.profile` (em alguns casos, pode ser necessário criar no `~/.bashrc` ou ainda no `/etc/environment`).

```shell
export JAVA_HOME=/usr/lib/jvm/11
```

Para garantir que a variável foi criada feche e abra novamente o terminal e execute o comando abaixo:

```shell
echo $JAVA_HOME
```

### macOS

Crie a variável de ambiente JAVA_HOME no `~/.bash_profile` ou `~/.zlogin` no macOS.

```shell
export JAVA_HOME=/Library/Java/JavaVirtualMachines/<pasta-da-versao-do-java>/Contents/Home
```

Para garantir que a variável foi criada feche e abra novamente o terminal e execute o comando abaixo:

``` shell
echo $JAVA_HOME
```

### Windows

Para adição do JAVA nas variáveis de Ambientes do Windows, siga os passos abaixo.

1. Em Pesquisar, procure e selecione: Sistema (Painel de Controle)
2. Clique no link Configurações avançadas do sistema.
3. Clique em Variáveis de Ambiente. Na seção Variáveis do Sistema clique em Novo e adicione a variável `JAVA_HOME` com o valor `C:\Program Files\Java\<pasta-da-versao-do-java>`.
4. Na seção Variáveis do Sistema localize a variável de ambiente `PATH` e selecione-a. Clique em Editar. Se a variável de ambiente PATH não existir, clique em Novo.
5. Na janela Editar Variável de Sistema (ou Nova Variável de Sistema), adicione um novo valor ```%JAVA_HOME%\bin``` que foi criada anteriormente. Clique em OK. Feche todas as janelas restantes clicando em OK.
6. Reabra a janela Prompt de comando e execute algum código Java ou o comando `java --version`.

---

# Instalação do Android Studio

Link para download do Android Studio: [https://developer.android.com/studio?hl=pt](https://developer.android.com/studio?hl=pt)

>**Observação para Linux:**
> 
>No **Linux** *(Ubuntu - imagem x86_64)* é possível instalar o Android Studio utilizando o `apt`, para isso execute os comandos abaixo:
>
> ```shell
> sudo add-apt-repository ppa:maarten-fonville/android-studio
> sudo apt-get install android-studio
>```
>
> Nas distros para processadores ARM, recomendamos a utilização do instalador oficial.

>**Observação para MacOs:**
> 
> No **macOS** também é possível instalar via Homebrew, basta executar o seguinte comando: `brew install android-studio`

>**Observação para Windows:**
> 
> Já no **Windows**, recomendamos a utilização do via instalador oficial.

## Passo-a-passo

[Faça o download](https://developer.android.com/studio?hl=pt) do Android Studio e siga o passo-a-passo para instalação conforme seu Sistema Operacional.

Após a instalação, abra o Android Studio e faça a instalação do Android SDK.

Para isso, abra o menu `Settings`, digite `Android SDK` na barra de pesquisa e selecione o caminho `Appearance & Behavior -> System Settings -> Android SDK`.

Selecione a aba `SDK Platform`.

Na listagem abaixo, marque a opção `Android API 32`.

Pressione o botão `Aplicar`.

Após a instalação, volte para a mesma tela e selecione a aba `SDK Tools`.

Marque os seguintes itens:

- [X] Android SDK Build-Tools
- [X] Android SDK Command-line Tools
- [X] Android Emulator
- [X] Android SDK Platform-tools
- [X] Google Play services
- [X] Google Web Driver
- [X] Intel x86 Emulator Accelerator (HAXM installer)
  - Caso você tenha escolhido esta opção, reinicie seu computador após a conclusão desta etapa.

Após selecionar todas as opções, pressione o botão `Aplicar`, aguarde a instalação e reinicie a IDE.

Volte para página `Settings`, digite `Plugins` na barra de pesquisa e selecione o caminho `Editor -> Plugins`.

Instale os seguintes plugins:

- Flutter
- Dart

Pressione o botão Aplicar`, aguarde e reinicie a IDE.

Pronto! IDE configurada.

## Configurando variáveis de ambiente para o Android SDK

Para finalizar a configuração do Android SDK, precisamos configurar as variáveis de ambiente, siga os passos abaixo conforme o seu sistema operacional.

### Linux

No arquivo `~/.bash_profile` ou `~/.profile` (alguns caso é necessário criar no `~/.bashrc` ou diretamente no `/etc/environment`), crie as variáveis de ambiente `ANDROID_HOME`, `ANDROID_SDK_ROOT` e modifique o `PATH` conforme a seguir:

```shell
export ANDROID_HOME="$HOME/Android/Sdk"
export ANDROID_SDK_ROOT=$ANDROID_HOME
export PATH="$HOME/android-studio/bin:$ANDROID_HOME:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH"
```

Para garantir que a variável foi criada feche e abra novamente o terminal e execute o comando abaixo:

```shell
echo $ANDROID_HOME
```

### macOS

No arquivo `~/.bash_profile` ou `~/.zlogin` no macOS, crie as variáveis de ambiente `ANDROID_HOME`, `ANDROID_SDK_ROOT` e adicione no PATH.

```shell
export ANDROID_HOME="$HOME/Library/Android/sdk"
export ANDROID_SDK_ROOT=$ANDROID_HOME
export PATH="$PATH:$ANDROID_HOME:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"
```

Para garantir que a variável foi criada feche e abra novamente o terminal e execute o comando abaixo:

```shell
echo $ANDROID_HOME
```

### Windows

Para adição de variáveis de Ambientes do Windows, siga os passos anteriormente descritos e crie as novas variáveis e adicione no `PATH`.

```shell
Variável: ANDROID_HOME
Valor: %USERPROFILE%\AppData\Local\Android\sdk
```

```shell
Variável: ANDROID_SDK_ROOT
Valor: %ANDROID_HOME%
```

>> **ATENÇÃO**: Se o Android SDK foi instalado pelo Android Studio, o local de instalação do Android SDK pode variar conforme configuração do caminho no momento da instalação.

Adicione à variável `PATH` os seguintes valores:

```
%ANDROID_HOME%
%ANDROID_HOME%/tools
%ANDROID_HOME%/platform-tools
```

---

# Instalação do Xcode (apenas macOS)

Para compilar uma aplicação Flutter para iOS, é necessário a instalação do Xcode e das respectivas ferramentas de linha de comando.

Para instalar abra o terminal e execute o comando abaixo:

```shell
xcode-select --install
```

Para utilizar o emulador de algum dispositivo com iOS, é necessário a instalação do Xcode.

 - Link para download: [Xcode na App Store](https://apps.apple.com/br/app/xcode/id497799835?mt=12)

---

# Instalação do Flutter

Abaixo separamos algumas instruções para ajudar no processo de instalação do Flutter. Caso queira seguir o guia oficial acesse: [Guia de instalação oficial](https://docs.flutter.dev/get-started/install).

## Linux

Vamos garantir que os pacotes do Linux estão atualizados. Para isso abra o terminal e execute o comando abaixo:

```shell
sudo apt-get update
```

Para instalar o Flutter no Linux, basta executar o comando abaixo:

```shell
sudo snap install flutter --classic
```

Após a instalação siga para a sessão **Pós-instalação**.

## macOS

Vamos garantir que o Homebrew está atualizado, para isso abra o terminal e execute o comando abaixo.

```shell
brew update
```

Para instalar o Flutter no macOS, basta executar o comando abaixo.

```shell
brew install flutter
```

Após a instalação siga para a sessão **Pós-instalação**.

## Windows

### Instalação via Chocolatey

Vamos garantir que o Chocolatey está atualizado, execute o **prompt de comando (CMD)** ou **PowerShell** como administrador e execute o comando abaixo.

```shell
choco upgrade chocolatey
```

Para instalar o Flutter no Windows, basta executar o comando abaixo:

```shell
choco install flutter --pre
```

Após a instalação siga para a sessão **Pós-instalação**.

### Instalação via Download Oficial

Faça o download do instalador oficial do Flutter no link: [https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_3.0.1-stable.zip](https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_3.0.1-stable.zip)

Extraia o conteúdo do zip para uma pasta sem utilizar caracteres especiais no caminho.

> **ATENÇÃO**: Não extrair o conteúdo do zip do flutter na pasta `C:\Program Files`

Após a instalação siga para a sessão **Pós-instalação**.

## Pós-instalação

### Configuração de variáveis de ambiente para o Flutter

Continuando a configuração do Flutter, precisamos configurar as variáveis de ambiente. Siga os passos abaixo conforme o seu sistema operacional.

#### Linux

>***ATENÇÃO:*** Se a instalação do Flutter no Linux foi feita seguindo os passos desse tutorial, não será necessário a configuração das variáveis ambientes.

#### macOS

No arquivo `~/.bash_profile` ou `~/.zlogin` no macOS, crie as variáveis de ambiente abaixo:

```shell
export FLUTTER_HOME="$HOMEBREW_PREFIX/Caskroom/flutter/3.0.1/flutter"
export DART_HOME="$FLUTTER_HOME/bin/cache/dart-sdk"
export PATH=$PATH:/usr/local/Caskroom/flutter/3.0.1/flutter/.pub-cache/bin
```

#### Windows

Para adição de variáveis de Ambientes do Windows, siga os passos anteriormente descritos e adicione no `PATH` o valor abaixo.

```shell
C:/<pasta-de-instalacao-do-flutter>/bin
```

--- 

### Executando o Flutter pela primeira vez

Para garantir que o Flutter foi instalado com sucesso, feche o terminal e abra novamente e execute o comando abaixo.

```shell
flutter --version
```

E em seguida para fazer uma verificação geral do ambiente de desenvolvimento e pré-requisitos do Flutter, execute o comando abaixo.

```shell
flutter doctor
```

Irá aparecer uma lista com os itens mínimos para iniciar o desenvolvimento com Flutter. Algo parecido com isso:

![Exemplo Flutter Doctor](assets/flutter-doctor-response.png)

> Caso apareça o item ``Chrome`` apareça marcado com **X**, não precise se preocupar, o foco desse guia é configurar o ambiente para desenvolvimento Mobile.
Caso queria desenvolver para Web, basta instalar o navegador [Google Chrome](Chrohttps://www.google.com/intl/pt-BR/chrome/me)

Continuando a configuração do Flutter, devemos aceitar as licenças do Android. Para tal, execute o comando: `flutter doctor --android-licenses` e todas as licensas digitando `Y` e apertando `Enter` para prosseguir.

>PRONTO!!!!

O Flutter está configurado na sua máquina e você está pronto para desenvolver aplicativos utilizando o Flutter.

--- 

# Instalação do GIT

## Linux

Vamos garantir que os pacotes do Linux estão atualizado, para isso abra o terminal e execute o comando abaixo:

```shell
sudo apt-get update
```

Para instalar o GIT no Linux basta executar o comando abaixo:

```shell
sudo apt-get install git
```

Para garantir que o GIT foi instalado com sucesso basta executar o comando a seguir. Caso o terminal não reconheça o comando, basta fechar o terminal e abrir novamente.

```shell
git --version
```

## macOS

> **ATENÇÃO**: Não é necessário instalar o GIT no macOS, pois o mesmo já é instalado quando da instalação do Xcode.

## Windows

### Instalação via Chocolatey

Para instalar via Chocolatey, basta executar o comando abaixo:

```shell
choco install git.install
```

### Instalação oficial

Baixe o instalar oficial no link [Instalador do GIT oficial](https://git-scm.com/downloads) e siga os passos.

Para garantir que o GIT foi instalado com sucesso basta executar o comando a seguir. Caso o terminal não reconheça o comando, basta fechar o terminal e abrir novamente.

```shell
git --version
```

PRONTO!!!!

Seu ambiente está configurado e apto para participação do [HACKATHON - RNP na Campus Party Goiás 02](https://cpgoias2.vaitercampus.org/#/paginas/hackathon-rnp).

Boa Hackathon!!!

---

# Links úteis

- [README.md do projeto Exemplo](README.md)
- [Documentação do Flutter](https://docs.flutter.dev/)
  - [Catálogo de Widgets](https://docs.flutter.dev/development/ui/widgets)
- [Indexador de Pacotes Dart](https://pub.dev)
- [Plugin oficial Adobe XD to Flutter](https://adobe.com/go/xd_to_flutter)
- [Coisas legais](https://github.com/Solido/awesome-flutter)
