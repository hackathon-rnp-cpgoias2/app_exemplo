<img src="assets/app-icon.png" width="128"/>

# eduplay

> Olá, pequeno Padawan!

Este projeto consiste num exemplo de adoção e uso da arquitetura mobile proposta para uso na futura Hackathon que será realizada nas edições Goiás e Brasil da Campus Party em 2022. Para tal, adota-se o uso da API do `eduplay`, em ambiente de desenvolvimento. Mas, vamos ao que interessa...

## Antes de começar...

Antes de começar, não deixe de conferir a mini-aula de Flutter e o [Guia de configuração do Ambiente](CONFIG.md).

Os slides apresentados na mini-aula podem ser obtidos [aqui](assets/ApresentacaoCP-HackathonEduplay.pdf).

## API

A documentação da API está disponível no [SwaggerHub](https://app.swaggerhub.com/apis/RNP1/Eduplay/1.0.0). Usar as credenciais:

- URL base: `https://dev.eduplay.rnp.br/services`
- Chave de API, no Header `clientKey`: `3247cb085ea21b1bfb9364437302d9ab34a037c38d3181fadee61bed38fbb963`

## Arquitetura

Para dar celeridade ao desenvolvimento e reduzir a curva de aprendizagem, algumas decisões arquiteturais foram tomadas para garantir a melhor performance das equipes durante o desafio.

Portanto, a arquitetura proposta adota os seguintes componentes:

<table border="0" style="width: 100%" align="center">
    <tr>
        <td>
            <img src="https://docs.flutter.dev/assets/images/shared/brand/flutter/logo/flutter-lockup.png" height="80"/>
        </td>
        <td>
            <img src="https://modular.flutterando.com.br/img/logo.png" height="80">
        </td>
        <td>
            <img src="https://raw.githubusercontent.com/felangel/bloc/master/docs/assets/bloc_logo_full.png" height="80"/>
        </td>
    </tr>
</table>


- [Flutter](https://flutter.dev), na sua versão mais recente `3.0.1 (stable)` quando da escrita deste;
  - E os frameworks correlatos:
    - 🇧🇷 [Flutter Modular](https://modular.flutterando.com.br/docs/intro/) - controle de rotas e injeção de dependências;
    - [Flutter BLoC](https://bloclibrary.dev/) - controle de estado

### Solução Proposta

Conhecidamente, o Flutter é um framework para concepção de aplicações híbridas, dotado de grande flexibilidade arquitetural, funcionando muitíssimo bem para aplicações de grande porte e tão bem quanto para aplicações pequenas.

Naturalmente, o uso da ferramenta difere em cada um dos cenários. Logo, optamos por estruturar os componentes da seguinte forma:

```plantuml
@startuml

package "eduplay" {

  cloud {
    () HTTP
  }

  package "App" {
    node "Flutter" {
      
      component Module {
        [Stateless Widget]
        [Statefull Widget]
        
        package "bloc" {
          [Cubit]
          [BLoC]
        }
      
      }
    
      package "Flutter Modular" {
        [Router]
        [Dependency Injection]
      }
      

      HTTP .. [Dio - HTTP]
      [Repository]

    }
  }
  
  node "API" {
    frame "Aplicação Backend" {
      [RESTful] .. HTTP
    }
  }
  
}

[Dio - HTTP] ..> [Repository] : Dependency Injection
[Repository] ..> [Cubit] : Dependency Injection
[Repository] ..> [BLoC] : Dependency Injection
Module --> [Router] : Flutter Modular
[Cubit] ..> [Statefull Widget] : Dependendy Injection
[Cubit] ..> [Stateless Widget] : Dependendy Injection
[BLoC] ..> [Statefull Widget] : Dependendy Injection
[BLoC] ..> [Stateless Widget] : Dependendy Injection
[Stateless Widget] .. [Router] : Utiliza
[Statefull Widget] .. [Router] : Utiliza

@enduml
```

- O `Flutter Modular` é um framework brasileiro, baseado no [Angular](https://angular.io), de onde busca inspiração para trazer o sistema de injeção de dependências e o sistema de rotas.
  - No sistema de rotas, vale mencionar, o _Modular_ emprega a emulação de uma árvore de componentes, exatamente como o próprio Flutter faz nativamente. Desta forma, é possível aninhar módulos;
  - Também a respeito do sistema de rotas, vale dizer que o _Modular_ faz o uso co `Navigator` do próprio Flutter para criar o efeito de múltiplos navegadores aninhados, possibilitando a divisão total por módulos independentes (o que acaba por fomentar o desenvolvimento de micro-frontends). Este recurso é chamado de `RouterOutlet`, assim como no _Angular_.
- `BLoC` é uma abreviação de _Business Logic Component_. Trata-se de uma biblioteca que nos auxilia na tarefa de gerenciar o estado - tanto da aplicação como um todo quando de cada _Widget_. O _BLoC_ foi criado, portanto, para corroborar com a separação de regras de negócio da implementação da interface. Como vantagens do _BLoC_, podemos dizer que ele torna fácil separar apresentação da lógica de negócio, deixando o código rápido, fácil de testar e reutilizável.
  - Dentro da biblioteca, podemos usar dois tipos principais de componente: o `Bloc` e o `Cubit`.
    - A diferença entre eles é que o `Bloc` é orientado a eventos, enquanto o `Cubit` (que é sua versão mais simples) é orientado a chamadas de função simples.
  - Então, temos os cenários:

![Bloc](https://bloclibrary.dev/assets/bloc_architecture_full.png)
  
~~~
~~~

![Cubit](https://bloclibrary.dev/assets/cubit_architecture_full.png)

#### Estrutura de pastas

O código-fonte de uma aplicação Flutter está contido na pasta `lib` do projeto. À partir dela, temos a seguinte estrutura proposta:

```shell
lib
├── app
│   ├── app_module.dart
│   ├── app_widget.dart
│   └── modules
│       ├── home
│       │   ├── home_module.dart
│       │   └── home_page.dart
│       ├── live
│       │   ├── bloc
│       │   │   ├── live_cubit.dart
│       │   │   ├── video_cubit.dart
│       │   │   └── video_version_cubit.dart
│       │   ├── live_module.dart
│       │   ├── live_page.dart
│       │   ├── video_play_page.dart
│       │   └── widgets
│       │       ├── live_list_widget.dart
│       │       └── video_list_widget.dart
│       └── shared
│           ├── model
│           │   └── cubit_state.dart
│           ├── repository
│           │   └── transmission_repository.dart
│           ├── shared_module.dart
│           ├── utils
│           │   └── constants.dart
│           └── widgets
│               └── bottom_navigation_bar_widget.dart
└── main.dart
```

#### Solução simplificada

A solução proposta não adota mecanismos extras de programação reativa, em especial o `rx_dart` - que teria um lugar bastante natural no desenho proposto da solução. 

Esta decisão foi tomada para deixar a solução proposta no maior nível de simplicidade possível, de forma a reduzir a curva de aprendizado ao menor nível, uma vez que esta é uma solução que poderá ser usada em uma _hackathon_, onde não haverá tempo hábil para aprender soluções complexas.

### Slidy

Para acelerar o desenvolvimento é indicada a adoção da ferramenta [Slidy](https://github.com/Flutterando/slidy). Trata-se de uma ferramenta de terminal, responsável por fornecer a funcionalidade de _scaffolding_ para diversos tipos de componente.

#### Instalação

Para instalar, o jeito mais fácil é adicionar um componente _Dart_ global:

```shell
flutter pub global activate slidy
```
<sup>Outras opções de instalação estão disponíveis no <a href="https://github.com/Flutterando/slidy" target="_blank">repositório do Slidy</a></sup>

#### Uso

Com o _Slidy_, podemos gerar:

> Não colocar sufixo ou prefixo indicando o tipo de componente, pois o Slidy já cuida disso.

- Módulos:
```shell
slidy g m modules/nome_do_modulo
```

- Cubit:
```shell
slidy g c modules/nome_do_modulo/bloc/nome_do_cubit
```

- Bloc:
```shell
slidy g b modules/nome_do_modulo/bloc/nome_do_bloc
```

- Repository:
```shell
slidy g r modules/nome_do_modulo/repository/nome_do_repository
```

- Páginas (Statefull Widget):
```shell
slidy g p modules/nome_do_modulo/nome_da_pagina
```

- Widgets (Stateless Widget):
```shell
slidy g w modules/nome_do_modulo/widgets/nome_do_widget
```

---

# Links úteis

- [Documentação do Flutter](https://docs.flutter.dev/)
  - [Catálogo de Widgets](https://docs.flutter.dev/development/ui/widgets)
- [Indexador de Pacotes Dart](https://pub.dev)
- [Plugin oficial Adobe XD to Flutter](https://adobe.com/go/xd_to_flutter)
- [Coisas legais](https://github.com/Solido/awesome-flutter)
