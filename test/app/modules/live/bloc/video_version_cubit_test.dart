import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:eduplay/app/modules/live/bloc/video_version_cubit.dart';
 
void main() {

  blocTest<VideoVersionCubit, int>('emits [1] when increment is added',
    build: () => VideoVersionCubit(),
    act: (cubit) => cubit.increment(),
    expect: () => [1],
  );
}