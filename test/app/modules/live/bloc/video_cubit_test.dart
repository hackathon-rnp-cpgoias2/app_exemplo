import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:eduplay/app/modules/live/bloc/video_cubit.dart';
 
void main() {

  blocTest<VideoCubit, int>('emits [1] when increment is added',
    build: () => VideoCubit(),
    act: (cubit) => cubit.increment(),
    expect: () => [1],
  );
}