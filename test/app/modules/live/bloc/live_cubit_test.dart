import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:eduplay/app/modules/live/bloc/live_cubit.dart';
 
void main() {

  blocTest<LiveCubit, int>('emits [1] when increment is added',
    build: () => LiveCubit(),
    act: (cubit) => cubit.increment(),
    expect: () => [1],
  );
}